package com.mycompany.myproject.java7;

public class StringSwitch {

    public static void main(String[] args) {

        final String currency = "PHP";
        final StringSwitch stringSwitch = new StringSwitch();
        System.out.println("Hello Dolf!");
        System.out.println(stringSwitch.getCurrencyName(currency));
		// test comment by Dolf
        // test for conflict
        System.out.println("Jaycobb Line");
    }

    private String getCurrencyName(final String code) {

        switch (code) {

            case "USD": {
                return "US Dollars";
            }
            case "GBP": {
                return "British Pounds";
            }
            case "PHP": {
                return "Philippine Peso";
            }
            default: {
                return "unknown";
            }
        }
    }

}
