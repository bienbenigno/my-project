package com.mycompany.myproject.java7;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TryWithResources {

    public static void main(String[] args) {

        TryWithResources tryWithResources = new TryWithResources();
        System.out.println(tryWithResources.readLinesNew());
    }

    private String readLinesOld() {

        String filename = "/Users/jaycobb/spacework/my-project/src/main/resources/test.txt";
        BufferedReader br = null;
        FileReader fileReader = null;
        StringBuilder lines = new StringBuilder();

        try {
            fileReader = new FileReader(filename);
            br = new BufferedReader(fileReader);

            String line;
            while ((line = br.readLine()) != null) {
                lines.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return lines.toString();
    }

    private String readLinesNew() {
        String filename = "/Users/jaycobb/spacework/my-project/src/main/resources/test.txt";
        StringBuilder lines = new StringBuilder();
        try (FileReader test = new FileReader(filename);
            BufferedReader buff = new BufferedReader(test)) {

            String line;
            while ((line = buff.readLine()) != null) {
                lines.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines.toString();
    }

}
