package com.mycompany.myproject.java8.functionalinterface;

import com.mycompany.myproject.java8.Person;

@FunctionalInterface
public interface NameProcessor {

    String process(Person person);

}
