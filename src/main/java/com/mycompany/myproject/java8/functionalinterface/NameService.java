package com.mycompany.myproject.java8.functionalinterface;

import com.mycompany.myproject.java8.Person;

public class NameService {

    public static void main(String[] args) {

        final NameProcessor firstNameFirst = person -> person.getFirstName() + " " + person.getLastName();
        final NameService nameService = new NameService();
        final Person person = new Person("John", "Doe", "X");

        final String lastNameFirst = nameService.get(person, nameService.lastNameFirst());
        System.out.println(lastNameFirst);

        final NameProcessor lastNameFirstNameAndMiddleName = nameService::lastNameFirstNameAndMiddleName;
        System.out.println(nameService.get(person, lastNameFirstNameAndMiddleName));
    }

    private String get(Person person, NameProcessor nameProcessor) {
        return nameProcessor.process(person);
    }

    private NameProcessor lastNameFirst() {

        return person -> person.getLastName() + ", " + person.getFirstName();
    }

    private String lastNameFirstNameAndMiddleName(Person person) {

        return person.getLastName() + ", " + person.getFirstName() + " " + person.getMiddleName();
    }

}
