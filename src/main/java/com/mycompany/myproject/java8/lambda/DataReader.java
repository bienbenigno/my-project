package com.mycompany.myproject.java8.lambda;

public interface DataReader {

    String read(Integer id);

}
