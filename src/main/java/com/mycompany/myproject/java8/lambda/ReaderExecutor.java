package com.mycompany.myproject.java8.lambda;

public class ReaderExecutor {

    public void execute( final DataReader dataReader ) {

        System.out.println("Opening database connection");

        System.out.println("Printing: '" + dataReader.read(23) + "'");

        System.out.println("Closing database connection");
    }

}
