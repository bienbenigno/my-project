package com.mycompany.myproject.java8.lambda;

public class Main {

    public static void main(String[] args) {

        final ReaderExecutor executor = new ReaderExecutor();

//        final DataReader dataReader = new DataReader() {
//
//            @Override
//            public String read(Integer id) {
//                return "a data from database with id " + id;
//            }
//        };
        final DataReader dataReader = id -> "a data from database with id " + id;

        executor.execute(dataReader);
    }

}
